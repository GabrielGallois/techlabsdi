package com.example.techlabsdi.util

object Constants {

    const val TIMEOUT = 30000L

    object Room {
        const val PHRASES_DATABASE_NAME = "PHRASES_DATABASE_NAME"
        const val PHRASES = "USER"
    }

    object Urls {
        const val BASE_URL = "https://api.chucknorris.io/"
        const val RANDOM = "jokes/random"
    }
}