package com.example.techlabsdi.util.ext


import android.content.Context

inline fun Context.showDialog(
    title: String,
    message: String,
    positiveText: String? = getString(android.R.string.ok),
    negativeText: String? = null,
    crossinline positiveClick: (() -> Unit) = {},
    crossinline negativeClick: (() -> Unit) = {},
    crossinline onDismiss: (() -> Unit) = {},
    isCancelable: Boolean = true
) {

    androidx.appcompat.app.AlertDialog.Builder(this).apply {
        setTitle(title)
        setMessage(message)
        setCancelable(isCancelable)
        positiveText?.let {
            setPositiveButton(it) { dialogInterface, _ ->
                positiveClick.invoke()
                dialogInterface.dismiss()
            }
        }
        negativeText?.let {
            setNegativeButton(it) { dialogInterface, _ ->
                negativeClick.invoke()
                dialogInterface.dismiss()
            }
        }
        setOnDismissListener {
            onDismiss.invoke()
        }
        show()
    }
}