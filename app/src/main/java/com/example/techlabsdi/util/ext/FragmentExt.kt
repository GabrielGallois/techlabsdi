package com.example.techlabsdi.util.ext

import androidx.fragment.app.Fragment

inline fun Fragment.showDialog(
    title: String,
    message: String,
    positiveText: String? = getString(android.R.string.ok),
    negativeText: String? = null,
    crossinline positiveButton: (() -> Unit) = {},
    crossinline negativeButton: (() -> Unit) = {},
    crossinline onDismiss: (() -> Unit) = {},
    isCancelable: Boolean = true,
) {
    requireContext().showDialog(
        title,
        message,
        positiveText,
        negativeText,
        positiveButton,
        negativeButton,
        onDismiss,
        isCancelable
    )
}