package com.example.techlabsdi.model

import com.squareup.moshi.Json

data class ChuckNorrisDTO(
    @Json(name = "value") val value: String
)
