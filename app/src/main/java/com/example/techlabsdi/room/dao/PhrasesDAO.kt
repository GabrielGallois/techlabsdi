package com.example.techlabsdi.room.dao

import androidx.room.*
import com.example.techlabsdi.room.entity.PhrasesEntity
import com.example.techlabsdi.util.Constants.Room
import kotlinx.coroutines.flow.Flow

@Dao
interface PhrasesDAO {

    @Query("SELECT * FROM ${Room.PHRASES} ORDER BY date DESC")
    fun getPhrases() : Flow<List<PhrasesEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(model: PhrasesEntity): Long

    @Delete
    suspend fun delete(model: PhrasesEntity)
}