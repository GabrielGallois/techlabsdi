package com.example.techlabsdi.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.techlabsdi.util.Constants
import java.util.*

@Entity(tableName = Constants.Room.PHRASES)
data class PhrasesEntity(
    @PrimaryKey
    @ColumnInfo(name = "date")
    val date: Date = Date(),

    @ColumnInfo(name = "phrase")
    val phrase: String,
)