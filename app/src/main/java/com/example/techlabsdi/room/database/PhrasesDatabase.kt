package com.example.techlabsdi.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.techlabsdi.room.dao.PhrasesDAO
import com.example.techlabsdi.room.database.converters.Converters
import com.example.techlabsdi.room.entity.PhrasesEntity
import com.example.techlabsdi.util.Constants

@Database(entities = [PhrasesEntity::class], version = 1, exportSchema = true)
@TypeConverters(Converters::class)
abstract class PhrasesDatabase : RoomDatabase() {

    abstract fun phrasesDao(): PhrasesDAO

    companion object {
        private const val DATABASE_NAME = Constants.Room.PHRASES_DATABASE_NAME

        @Volatile
        var instance: PhrasesDatabase? = null

        fun clearInstance() {
            instance = null
        }
    }
}