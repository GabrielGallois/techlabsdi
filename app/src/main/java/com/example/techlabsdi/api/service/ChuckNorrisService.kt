package com.example.techlabsdi.api.service

import com.example.techlabsdi.model.ChuckNorrisDTO
import com.example.techlabsdi.util.Constants
import retrofit2.http.GET

interface ChuckNorrisService {

    @GET(Constants.Urls.RANDOM)
    suspend fun fetchChuckNorrisPhrase(): ChuckNorrisDTO
}