package com.example.techlabsdi.view

import android.os.Bundle
import android.os.PersistableBundle
import androidx.activity.viewModels
import com.example.techlabsdi.R
import com.example.techlabsdi.databinding.ActivityMainBinding
import com.example.techlabsdi.view.base.BaseActivity
import com.example.techlabsdi.view.viewmodel.PhrasesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity: BaseActivity<ActivityMainBinding>(R.layout.activity_main) {

    private val userViewModel: PhrasesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

    }
}