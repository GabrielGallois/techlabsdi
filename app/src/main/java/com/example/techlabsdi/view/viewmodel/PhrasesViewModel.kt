package com.example.techlabsdi.view.viewmodel

import androidx.lifecycle.*
import com.example.techlabsdi.repository.ChuckNorrisRepository
import com.example.techlabsdi.room.entity.PhrasesEntity
import com.example.techlabsdi.util.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhrasesViewModel
@Inject
constructor(
    private val repository: ChuckNorrisRepository
) : ViewModel() {

    private val _onChuckNorrisPhraseFetched = MutableLiveData<String>()
    val onChuckNorrisPhraseFetched: LiveData<String> = _onChuckNorrisPhraseFetched

    private val _onChuckNorrisPhraseFetchError = MutableLiveData<Event<Throwable>>()
    val onChuckNorrisPhraseFetchError: LiveData<Event<Throwable>> = _onChuckNorrisPhraseFetchError

    private val _onLoading = MutableLiveData<Event<Unit>>()
    val onLoading: LiveData<Event<Unit>> = _onLoading

    private val _onLoadFinish = MutableLiveData<Event<Unit>>()
    val onLoadFinish: LiveData<Event<Unit>> = _onLoadFinish

    val allPhrases: LiveData<List<PhrasesEntity>> = repository.allPhrases.asLiveData()

    fun insert(phrase: PhrasesEntity) = viewModelScope.launch {
        repository.insert(phrase)
    }

    fun removePhraseAt(position: Int) = viewModelScope.launch {
        repository.delete(allPhrases.value?.get(position) ?: return@launch)
    }

    fun fetchChuckNorrisPhrase() {
        viewModelScope.launch {
            _onLoading.postValue(Event(Unit))
            try {
                val response = repository.fetchChuckNorrisPhrase()
                _onChuckNorrisPhraseFetched.value = response.value

            } catch (throwable: Throwable) {
                _onChuckNorrisPhraseFetchError.value = Event(throwable)
            }
            _onLoadFinish.postValue(Event(Unit))
        }
    }
}