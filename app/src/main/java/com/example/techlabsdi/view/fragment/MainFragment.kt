package com.example.techlabsdi.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import com.example.techlabsdi.R
import com.example.techlabsdi.databinding.FragmentMainBinding
import com.example.techlabsdi.room.entity.PhrasesEntity
import com.example.techlabsdi.util.EventObserver
import com.example.techlabsdi.util.ext.showDialog
import com.example.techlabsdi.view.adapter.PhrasesAdapter
import com.example.techlabsdi.view.base.BaseFragment
import com.example.techlabsdi.view.viewmodel.PhrasesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : BaseFragment<FragmentMainBinding>(R.layout.fragment_main) {

    private val userViewModel: PhrasesViewModel by activityViewModels()

    private lateinit var adapter: PhrasesAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userViewModel.fetchChuckNorrisPhrase()

        adapter = PhrasesAdapter(onRemove = {
            userViewModel.removePhraseAt(it)

//            showDialog(
//                getString(R.string.attention),
//                getString(R.string.remove_user_confirmation),
//                negativeText = getString(R.string.cancel),
//                positiveButton = {
//                    userViewModel.removeUserAt(it)
//                })
        })

        binding.recyclerView.adapter = adapter

        binding.txtDescription.setOnClickListener {
            userViewModel.fetchChuckNorrisPhrase()
        }

        binding.btnAdd.setOnClickListener {
            userViewModel.insert(PhrasesEntity(phrase = binding.txtDescription.text?.toString() ?: return@setOnClickListener))
        }

        userViewModel.allPhrases.observe(viewLifecycleOwner) { response ->
            adapter.models = response
        }

        userViewModel.onChuckNorrisPhraseFetched.observe(viewLifecycleOwner) {
            binding.txtDescription.text = it
        }

        userViewModel.onChuckNorrisPhraseFetchError.observe(viewLifecycleOwner, EventObserver { throwable ->
            throwable.message?.let {
                showDialog(getString(R.string.error), it)
            }
        })

        userViewModel.onLoading.observe(viewLifecycleOwner, EventObserver {
            binding.progressBar.visibility = View.VISIBLE
            binding.txtDescription.visibility = View.GONE
            binding.btnAdd.isClickable = false
        })

        userViewModel.onLoadFinish.observe(viewLifecycleOwner, EventObserver {
            binding.progressBar.visibility = View.GONE
            binding.txtDescription.visibility = View.VISIBLE
            binding.btnAdd.isClickable = true
        })
    }
}