package com.example.techlabsdi.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.techlabsdi.R
import com.example.techlabsdi.databinding.ItemSimpleAdapterBinding
import com.example.techlabsdi.room.entity.PhrasesEntity

class PhrasesAdapter(
    private val onClick: (position: Int) -> Unit = {},
    private val onRemove: (position: Int) -> Unit = {}
) : RecyclerView.Adapter<PhrasesAdapter.PhrasesViewHolder>() {

    var models: List<PhrasesEntity> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhrasesViewHolder {
        return PhrasesViewHolder(ItemSimpleAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: PhrasesViewHolder, position: Int) {
        holder.binding.apply {
            textTitle.text = root.context.getString(R.string.fact)

            textDescription.text = models[position].phrase
        }
    }

    override fun getItemCount() = models.size

    inner class PhrasesViewHolder(val binding: ItemSimpleAdapterBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            binding.root.setOnClickListener(this)
            binding.btnRemove.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = absoluteAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                when(v) {
                    binding.root -> {
                        onClick.invoke(position)
                    }
                    binding.btnRemove -> {
                        onRemove.invoke(position)
                    }
                }
            }
        }
    }
}