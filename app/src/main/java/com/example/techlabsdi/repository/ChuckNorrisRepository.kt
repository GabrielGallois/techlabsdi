package com.example.techlabsdi.repository

import com.example.techlabsdi.api.service.ChuckNorrisService
import com.example.techlabsdi.model.ChuckNorrisDTO
import com.example.techlabsdi.room.dao.PhrasesDAO
import com.example.techlabsdi.room.entity.PhrasesEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ChuckNorrisRepository
@Inject
constructor(
    private val chuckNorrisService: ChuckNorrisService,
    private val phrasesDao: PhrasesDAO
) {
    val allPhrases: Flow<List<PhrasesEntity>> = phrasesDao.getPhrases()

    suspend fun fetchChuckNorrisPhrase(): ChuckNorrisDTO = chuckNorrisService.fetchChuckNorrisPhrase()

    suspend fun delete(phrase: PhrasesEntity) = phrasesDao.delete(phrase)

    suspend fun insert(user: PhrasesEntity): Long {
        return phrasesDao.insert(user)
    }
}