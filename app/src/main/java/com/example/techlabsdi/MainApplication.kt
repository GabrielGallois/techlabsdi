package com.example.techlabsdi

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication: Application() {

    init {
        instance = this
    }

    companion object {
        private lateinit var instance: MainApplication

        fun applicationContext(): Context {
            return instance.applicationContext
        }
    }
}

