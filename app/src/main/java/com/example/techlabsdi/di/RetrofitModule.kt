package com.example.techlabsdi.di

import com.example.techlabsdi.BuildConfig
import com.example.techlabsdi.api.service.ChuckNorrisService
import com.example.techlabsdi.util.Constants
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton

/**Hilt component for singleton scope bindings and all scopes below its hierarchy:
 * SingletonComponent -> ActivityRetainedComponent -> ActivityComponent -> FragmentComponent -> ViewWithFragmentComponent
 * this component is destroyed only when the Application is destroyed
 * older Hilt equivalent to SingletonComponent::class is ApplicationComponent::class
 * more detailed info here: https://dagger.dev/hilt/components
 * */
@InstallIn(SingletonComponent::class)

@Module
class RetrofitModule {

    /** @BaseUrl is a @Qualifier annotation
     * it must be used when you provide a type more than once
     * If @Qualifier is created it must be used inside constructor
     * */
    @BaseUrl
    @Provides
    fun baseUrl() = Constants.Urls.BASE_URL

    @WrongUrl
    @Provides
    fun wrongUrl() = Constants.Urls.RANDOM

    /** By default, all bindings in Dagger are “unscoped”.
     * This means that each time the binding is requested, Dagger will create a new instance of the binding.
     * A scoped binding will only be created once per instance of the component it’s scoped to,
     * and all requests for that binding will share the same instance.
     * to add scope just add a scope anotation:
     * @Singleton, @ActivityRetainedScoped, @ViewModelScoped, @ActivityScoped, @FragmentScoped,
     * @ViewScoped, @ServiceScoped
     * */
    @Provides
    fun provideOkHttpClient() =
        if (BuildConfig.DEBUG) {

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            OkHttpClient.Builder()
                .readTimeout(Constants.TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(Constants.TIMEOUT, TimeUnit.MILLISECONDS)
                .connectTimeout(Constants.TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(loggingInterceptor)
                .build()
        } else {
            OkHttpClient.Builder()
                .readTimeout(Constants.TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(Constants.TIMEOUT, TimeUnit.MILLISECONDS)
                .connectTimeout(Constants.TIMEOUT, TimeUnit.MILLISECONDS)
                .build()
        }

    /** Here is an example using a scope annotation
     * now provideRetrofit will be destroyed only when Application is destroyed
     * it is optional
     * */
    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        @BaseUrl BASE_URL: String
    ): Retrofit = Retrofit.Builder().run {
        baseUrl(BASE_URL)

        val kotlinJsonAdapter = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()

        addConverterFactory(MoshiConverterFactory.create(kotlinJsonAdapter))
        client(okHttpClient)
        build()
    }

    @Provides
    @Singleton
    fun provideChuckNorrisService(retrofit: Retrofit) =
        retrofit.create(ChuckNorrisService::class.java)

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class BaseUrl

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class WrongUrl
}