package com.example.techlabsdi.di

import android.content.Context
import androidx.room.Room
import com.example.techlabsdi.room.dao.PhrasesDAO
import com.example.techlabsdi.room.database.PhrasesDatabase
import com.example.techlabsdi.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun providePhrasesDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, PhrasesDatabase::class.java, Constants.Room.PHRASES_DATABASE_NAME)
            .addMigrations()
            .build()

    @Singleton
    @Provides
    fun provideUserDao(phrasesDatabase: PhrasesDatabase): PhrasesDAO {
        return phrasesDatabase.phrasesDao()
    }
}